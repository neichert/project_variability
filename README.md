# General information
* Code in this repository relates to the following publication: **Eichert N, Watkins KE, Mars RB & Petrides M (2020, in press) Morphological and functional variability in central and subcentral motor cortex of the human brain. Brain Structure and Function. doi: 10.1007/s00429-020-02180-w**

* Preprint available on bioRxiv ([doi: 10.1101/2020.03.17.995035](https://www.biorxiv.org/content/10.1101/2020.03.17.995035v3))

* Data related to this publication is deposited at OpenNeuro under the Accession number [ds002634](https://openneuro.org/datasets/ds002634)

* Preprocessing of structural and functional data related to this publication is partly described in a related code repository: [https://git.fmrib.ox.ac.uk/neichert/project_larynx](https://git.fmrib.ox.ac.uk/neichert/project_larynx)

* For any questions, please don't hesitate to contact Nicole Eichert at [nicole.eichert@psy.ox.ac.uk](mailto:nicole.eichert@psy.ox.ac.uk)

# Comments on the repository
* Subject-IDs 01 - 20 refer to the subjects that contributed both structural and functional data
* Subject-IDs 21 - 50 refer to the HCP subjects that only contributed structural data
* not all scripts run directly on the OpenNeuro dataset because it does not contain all intermediate data
* all missing intermediate data can, in principle, be reconstructed using the provided code, but file paths might have to be adapted

# Comments on specific scripts
* process_sulci.sh:
    - takes as starting point the manually drawn sulcal borders on each subject's native surface
    - performs basic processing and resamples to standard space
    - generates group-level probability maps
* process_sulci_wrapper.sh:
    - wrapper script to call the different steps of the above script
* quantify_anatomical_features.py:
    - extract anatomical measures for each sulcal label
    - derive voxel of maximal overlap and centre of gravity for group level
    - plot the anatomical measures
* MSM_registration.sh:
    - derive registrations based on sulcal labels and based on sulcal depth maps
    - resamples functional peaks based on these registrations 
* get_distance.py:
    - computes the pair-wise distance across all subjects for a given effector and registration
* get_distance_wrapper.sh
    - calls the above script and reads in the results for all effectors
    - generates a plot of distances across effectors and registrations
* set_colour_labels.py
    - modify colours in labels according to fixed RGB values
* post_processing_wrapper.sh
    - calls the above scripts in a given order
* FSLeyes_probability_maps.ipynb
    - interactive jupyter notebook to generate figures for probability maps
* FSLeyes_sulci_3D.ipynb
    - 3D rendering of the labels of a single subject
    - the output of one example is provided in this repository as `sulci_individual.gif`
* FSLeyes_sulci_individual.ipynb
    - load volumetric labels in one individual and set colour maps
     
# How to reproduce figures and tables from publication
* Fig. 1B - Inividual sulci:
    - run `FSLeyes_sulci_individual.ipynb` for sub-20
* Fig. 3 - Probability maps:
    - run `FSLeyes_probability_maps.ipynb`
* Fig. 4 - Anatomical measures:
    - run `quantify_anatomical_features.py`
    - the script directly read in provided csv file `anatomical_quantifications.csv` (set rerun=0)
    - or derive anatomical measures again: set rerun=1
* Fig. 5 - All individual sulci and peaks
    - run `process_sulci.sh` to get borders on common sphere
    - run `func_ROIs.sh` from project_larynx repository to get all task peaks on common sphere
    - load data in wb_view 
* Fig. 6 - Effect of registrations
    - A:
        - run `MSM_registration.sh`
        - run `get_distance_wrapper.py run` to get distances across peaks
        - run `get_distance_wrapper.py load` to read in distances and reshape
        - run `get_distance_wrapper.py plot` to generate figure
        - the last step reads in the provided csv file `peak_distances.csv`
    - B: 
        - run `func_ROIs.sh` from project_larynx repository to get combined file of peaks
* Fig. 7 (Supplementary Material) - Anatomical measures per morphological type
    - run `quantify_anatomical_features.py`
* Table 1 - Morphological types
    - numbers can be inferred from the provided csv file `structure_function_links.csv`
    - run `quantify_anatomical_features.py` last section
* Table 2 - Coordinates
    - run `quantify_anatomical_features.py` and read out from dataframe
* Table 3 - Structure-function-relationships
    - numbers can be inferred from the provided csv file `structure_function_links.csv`
    - run `quantify_anatomical_features.py` last section    
    