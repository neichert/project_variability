import nibabel as nib
import numpy as np
import sys
import os
import nibabel as nib
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import subprocess
import pdb
from scipy.stats import ttest_rel
from statsmodels.stats.multicomp import pairwise_tukeyhsd, MultiComparison

FSLDIR = '/usr/local/fsl'
rootdir = os.path.join(os.sep, 'Users', 'neichert', 'public_data', 'project_larynx')
GD = os.path.join(rootdir, 'derivatives', 'group')


subj_list = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
             '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
             '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
             '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
             '41', '42', '43', '44', '45', '46', '47', '48', '49', '50']

hemi_list = ['L', 'R']
feature_list = ['g_surf_prob', 'g_mni_max', 'g_mni_cog', 'thickness', 'depth', 'size_surf']

# define colour mapping for sulci
selection = np.array([ [0.  , 0.6 , 1.  ],
                       [0.  , 0.7 , 0.  ],
                       [0.51, 0.47, 0.81],
                       [0.51, 0.55, 0.  ],
                       [0.56, 0.5 , 0.34],
                       [0.82, 0.42, 0.34],
                       [1.  , 0.38, 0.17],
                       [1.  , 0.34, 0.64]])

sulcus_dict = {'cs_1': selection[4],
               'cs_2': selection[2],
               'cs_3': selection[6],
               'cs_4': selection[3],
               'cs_5': selection[7],
               'ascs_lat': selection[0],
               'ascs_op': selection[1],
               'pscs': selection[5]}

# read in info about morphological subtype per subject
type_df = pd.read_csv(os.path.join(GD, 'anat', 'sulci', 'structure_function_links.csv'))

# load std brain
fname_std = os.path.join(FSLDIR, 'data', 'standard', 'MNI152_T1_0.5mm.nii.gz')
img = nib.load(fname_std)

# initialize group level dataframe
df_g = pd.DataFrame(columns=['hemi', 'sulcus', 'feature', 'value'])
my_row_g = 0

# initialize subject level dataframe
df_i = pd.DataFrame(columns=['hemi', 'sulcus', 'feature', 'subj', 'value', 'subtype'])
my_row_i = 0

# If 1: derive anatomical measures and derive summary csv
# If 0: only do plotting
rerun = 0

if rerun:
    for hemi in hemi_list:
        for i_s, (sc, colour) in enumerate(sulcus_dict.items()):
            # load probability maps
            sc_surf_prob = nib.load(os.path.join(GD, 'anat', 'sulci', f'sulci_{hemi}_probability.func.gii')).darrays[i_s].data
            sc_vol_prob = nib.load(os.path.join(GD, 'anat', 'sulci', f'{sc}_{hemi}_probability.nii.gz')).get_fdata()

            for feature in feature_list:
                # for group level features g_
                if feature[0:2] == 'g_':

                    # get maximal value in surface probability map
                    if feature == 'g_surf_prob':
                        my_val = np.int(np.max(sc_surf_prob[sc_surf_prob != 0]) / 50 * 100)

                    # get maximal value in volume probability map
                    elif feature == 'g_vol_prob':
                        my_val = np.int(np.max(sc_vol_prob[sc_vol_prob != 0]) / 50 * 100)

                    # get voxel of maximal probability in volume map
                    elif feature == 'g_mni_max':
                        # get maximal voxel in MNI space
                        vox_coords = np.where(sc_vol_prob == np.max(sc_vol_prob))
                        vox_coords = np.array([vox_coords[0][0], vox_coords[1][0], vox_coords[2][0]])
                        my_val = np.round(nib.affines.apply_affine(img.affine, vox_coords))

                    # get center of gravity in volume map
                    elif feature == 'g_mni_cog':
                        # get cog voxel from file
                        proc = subprocess.Popen(f"fslstats {GD}/anat/sulci/{sc}_{hemi}_probability.nii.gz -C", stdout=subprocess.PIPE, shell=True)
                        fsl_cog = proc.communicate()[0]
                        cog = np.round(np.array(str(fsl_cog)[2:-4].split(' ')).astype(np.float)).astype(np.int)
                        my_val = np.round(nib.affines.apply_affine(img.affine, cog)).astype(int)

                    # append dataframe
                    df_g.loc[my_row_g] = [hemi, sc, feature, my_val]
                    my_row_g = my_row_g + 1

                # for features on the single subject level
                else:
                    for sub in subj_list:
                        subj = 'sub-' + sub
                        OD = os.path.join(rootdir, 'derivatives', subj)
                        # load in sulcal map
                        sc_map = nib.load(os.path.join(OD, 'sulci_native_surf', f'sulci_{hemi}.func.gii')).darrays[i_s].data
                        # load info about morphological subtype
                        subtype = type_df[(type_df.subj == subj) & (type_df.hemi == hemi)].subtype.values[0]

                        if feature == 'thickness':
                            thickness_map = nib.load(os.path.join(OD, 'MNINonLinear', 'Native', f'{subj}.{hemi}.thickness.native.shape.gii')).darrays[0].data
                            my_val = np.mean(thickness_map[sc_map > 0])

                        elif feature == 'depth':
                            depth_map = nib.load(os.path.join(OD, 'MNINonLinear', 'Native', f'{subj}.{hemi}.sulc.native.shape.gii')).darrays[0].data
                            my_val = np.min(depth_map[sc_map > 0])

                        elif feature == 'size_surf':
                            my_val = np.sum(sc_map > 0)

                        # append dataframe
                        df_i.loc[my_row_i] = [hemi, sc, feature, subj, my_val, subtype]
                        my_row_i = my_row_i + 1

    # merge group-level features with averaged individual features
    df_i_avg = df_i.groupby(['hemi', 'sulcus', 'feature']).mean().reset_index()
    df = pd.concat([df_g, df_i_avg], ignore_index=True)

    # merge individual features with group-level features in one combined dataframe
    df_g["subj"] = np.nan
    df_g["subtype"] = np.nan
    df2 = pd.concat([df_g, df_i], ignore_index=True)

    # get relationships for tables
    df_table_i = pd.pivot_table(df_i, index=['hemi', 'sulcus', 'subj'], columns=['feature'], values=['value', 'subtype']).reset_index()
    df_table = pd.pivot_table(df, index=['hemi', 'sulcus'], columns=['feature'], values='value', aggfunc=np.sum).reset_index()

    # exclude max and cog coordinates for plotting
    df_plot = df2[(df2.feature != 'g_mni_max') & (df2.feature != 'g_mni_cog')]

    # write out numbers for plotting to csv
    df_plot.to_csv(os.path.join(GD, 'sulci', 'anatomical_quantifications.csv'), float_format='%.2f', index=False)

# PLOTTING
df_plot = pd.read_csv(os.path.join(GD, 'anat', 'sulci', 'anatomical_quantifications.csv'))
# generate plot of anatomical features
p1 = sns.catplot(data=df_plot, hue='hemi', x="sulcus", order=sulcus_dict.keys(), y='value', row='feature', kind='bar', sharey=False, legend=False, aspect=1)
for ax in p1.axes:
    for ind_sc, (sc, colour) in enumerate(sulcus_dict.items()):
        ax[0].patches[ind_sc].set_facecolor(colour)
        ax[0].patches[ind_sc + len(sulcus_dict)].set_facecolor(colour)
        ax[0].patches[ind_sc + len(sulcus_dict)].set_hatch('///')
plt.show()

# generate of quantifications plot per subtype (supplementary figure)
# exclude coordinates for plotting
df_plot2 = df_plot[(df_plot.feature != 'g_surf_prob')]
ft_list = ['thickness', 'depth', 'size_surf']
st_list = ['Type_1', 'Type_2', 'Type_3', 'Type_4', 'Type_5']
p2 = sns.catplot(data=df_plot2, hue='hemi', x="sulcus", order=sulcus_dict.keys(), y='value', row='feature', row_order=ft_list, kind='bar', col='subtype', col_order=st_list, sharey=False, legend=False)
[n_ft, n_st] = p2.axes.shape
for ft, feature in enumerate(ft_list):
    for st in np.arange(0, n_st):
        ax = p2.axes[ft][st]
        if feature == 'thickness':
            ax.set_ylim([0, 3.5])
        if feature == 'sulc_size':
            ax.set_ylim([0, 2100])
        if feature == 'depth':
            ax.set_ylim([-2, 0.5])
        ax.set_yticklabels("")
        ax.set_xticklabels("")
        ax.set_title("")
        for ind_sc, (sc, colour) in enumerate(sulcus_dict.items()):
            ax.patches[ind_sc].set_facecolor(colour)
            ax.patches[ind_sc + len(sulcus_dict)].set_facecolor(colour)
            ax.patches[ind_sc + len(sulcus_dict)].set_hatch('///')
plt.show()

### Summary tables
# Table 1
print(type_df.groupby(['type_of_subcentral_region', 'hemisphere']).size())

# Table 3
print(type_df.groupby(['hemisphere', 'ROI', 'sulcal_label']).size())


