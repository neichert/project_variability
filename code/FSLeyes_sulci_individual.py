import os
import sys
import numpy as np
from matplotlib.colors import LinearSegmentedColormap

rootdir = os.path.join(os.sep, 'Users', 'neichert', 'public_data', 'project_larynx')
sulci_list = ['cs_1', 'cs_2', 'cs_3', 'cs_4', 'cs_5', 'ascs_lat', 'ascs_op', 'pscs']

selection = np.array([[0.  , 0.6 , 1.  ],
                       [0.  , 0.7 , 0.  ],
                       [0.51, 0.47, 0.81],
                       [0.51, 0.55, 0.  ],
                       [0.56, 0.5 , 0.34],
                       [0.82, 0.42, 0.34],
                       [1.  , 0.38, 0.17],
                       [1.  , 0.34, 0.64]])

colour_dict = {'cs_1': selection[4],
               'cs_2': selection[2],
               'cs_3': selection[6],
               'cs_4': selection[3],
               'cs_5': selection[7],
               'vl': selection[0],
               'fo4': selection[1],
               'fo5': selection[5]}

# -----
# load files to display
# -----
subj = '20'
OD = os.path.join(rootdir, 'derivatives', 'sub-' + subj)

# load anatomical in MNI space
load(os.path.join(OD, 'MNINonLinear', 'T1w_restore_brain.nii.gz'))

# load sulci
for sc in sulci_list:
    for hemi in ['L']:
        load(os.path.join(OD, 'sulci_LR32k', sc + '_' + hemi + '_s.nii.gz'))

# set colour
for overlay in overlayList:
    if '_L_s' in overlay.name:
        this_colour = [value for key, value in colour_dict.items() if key in overlay.name][0]
        cmap = LinearSegmentedColormap.from_list('mycmap', ['black', this_colour, this_colour])
        displayCtx.getOpts(overlay).cmap = cmap
        displayCtx.getOpts(overlay).clippingRange = (np.max(overlay.data)*0.1, np.max(overlay.data))
        displayCtx.getOpts(overlay).displayRange = (np.max(overlay.data)*0.1, np.max(overlay.data))