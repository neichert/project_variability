import numpy as np
import os
import sys
import subprocess
import pandas as pd
import nibabel as nib


def main():
    # hemisphere
    hemi = sys.argv[1]
    # type of effector
    peak = sys.argv[2]
    # type of registration
    type = sys.argv[3]
    print(hemi, peak, type)
    OSTYPE = sys.platform
    if OSTYPE == "darwin":
        myscratch = os.path.join(os.sep, 'Users', 'neichert', 'scratch')
    else:
        myscratch = os.path.join(os.sep, 'vols', 'Scratch', 'neichert')
    GD = os.path.join(rootdir, 'derivatives', 'group')
    subj_list = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '11', '10', '12', '13', '14', '15', '16', '17', '18', '19', '20']
    distances_fname = os.path.join(GD, 'func', peak + "_" + type + "_" + hemi + "_distances.pkl")
    df = pd.DataFrame(columns=['subject1', 'subject2', 'distance'])
    my_row = 0
    ref_sphere = os.path.join(rootdir, 'derivatives', 'sub-01', 'MNINonLinear', 'fsaverage_LR32k', f'sub-01.{hemi}.sphere.32k_fs_LR.surf.gii')

    for subj1 in subj_list:
        print(subj1)
        OD1 = os.path.join(rootdir, 'derivatives', 'sub-' + subj1)
        peak1 = os.path.join(OD1, f'reg_{type}', f'{peak}_{hemi}_peak.func.gii')
        for subj2 in subj_list:
            OD2 = os.path.join(rootdir, 'derivatives', 'sub-' + subj2)
            peak2 = os.path.join(OD2, f'reg_{type}', f'{peak}_{hemi}_peak.func.gii')
            vx_peak1 = np.argmax(nib.load(peak1).darrays[0].data)
            out_file = os.path.join(GD, 'func', subj1 + subj2 + hemi + peak + type + '.func.gii')
            os.system("wb_command -surface-geodesic-distance " + ref_sphere + " " + str(vx_peak1) + " " + out_file)
            p2 = subprocess.Popen("wb_command -metric-stats " + out_file + " -reduce MEAN -roi " + peak2, stdout=subprocess.PIPE, shell=True)
            dist = np.float(p2.communicate()[0])
            df.loc[my_row] = [subj1, subj2, dist]
            my_row = my_row + 1
            os.remove(out_file)

    print("save: ", distances_fname)
    df.to_pickle(distances_fname)

main()
