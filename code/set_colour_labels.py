import xml.etree.ElementTree as ET
import numpy as np
import os
from matplotlib import colors

# RGB colours
selection = np.array([[0.  , 0.6 , 1.  ],
                       [0.  , 0.7 , 0.  ],
                       [0.51, 0.47, 0.81],
                       [0.51, 0.55, 0.  ],
                       [0.56, 0.5 , 0.34],
                       [0.82, 0.42, 0.34],
                       [1.  , 0.38, 0.17],
                       [1.  , 0.34, 0.64]])

colour_dict = {'cs_1': selection[4],
               'cs_2': selection[2],
               'cs_3': selection[6],
               'cs_4': selection[3],
               'cs_5': selection[7],
               'ascs_lat': selection[0],
               'ascs_op': selection[1],
               'pscs': selection[5]}

subj_list = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
             '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
             '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
             '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
             '41', '42', '43', '44', '45', '46', '47', '48', '49', '50']

# set the colour of a specific border
def set_colour(border, colour_dict):
    sc = [item[1] for item in border.items() if 'Name' in item][0]
    if sc[0:-2] in colour_dict.keys():
        wanted_colour = [value for key, value in colour_dict.items() if key in sc][0]
    else:
        wanted_colour = [0, 0, 0]
    rgb = colors.to_rgba(wanted_colour)
    border.set('Red', str(rgb[0]))
    border.set('Green', str(rgb[1]))
    border.set('Blue', str(rgb[2]))


# change colour for group-level sulci
for hemi in ['L', 'R']:
    fname = os.path.join(rootdir, 'derivatives', 'group', 'anat', f'sulci_{hemi}_avg_1.border')
    tree = ET.parse(fname)
    root = tree.getroot()
    # TODO: make this loop more general
    for border_child in root.getchildren():
        has_items = border_child.items()
        if has_items:
            set_colour(border_child, colour_dict)
    tree.write(fname)


    # do all subjects in combined group-level file
    fname = os.path.join(rootdir, 'derivatives', 'group', 'sulci', f'sulci_{hemi}.border')
    tree = ET.parse(fname)
    root = tree.getroot()
    for border_child in root.getchildren():
        has_items = border_child.items()
        if has_items:
            for border_child_child in border_child.getchildren():
                set_colour(border_child_child, colour_dict)
    tree.write(fname)

    # do all borders of individual subjects on native surface
    for sub in subj_list:
        if sub[0] == 'h':
            subj = 'subj' + sub[1:]
        else:
            subj = 'sub-' + sub
        print(subj)
        OD = os.path.join(rootdir, 'derivatives', subj)
        fname = os.path.join(OD, 'sulci_native_surf', f'sulci_{hemi}.border')
        tree = ET.parse(fname)
        root = tree.getroot()
        for border_child in root.getchildren():
            has_items = border_child.items()
            if has_items:
                for border_child_child in border_child.getchildren():
                    set_colour(border_child_child, colour_dict)
        tree.write(fname)