
# get ventral ROI
subj_list='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20'
for subj in $subj_list; do
    for hemi in L R; do
        bash process_sulci.sh $subj ventral_ROI $hemi
    done
done

# get individual sulci
sc_list='cs_1 cs_2 cs_3 cs_4 cs_5 ascs_lat ascs_op pscs'
subj_list='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 30 40 41 42 43 44 45 46 47 48 49 40 41 42 43 44 45 46 47 48 49 50'
for sc in $sc_list; do
     for hemi in L R; do
        for subj in $subj_list; do
            bash process_sulci.sh $subj $sc $hemi
        done
        # get surface and volume probability maps
        bash process_sulci.sh group $sc $hemi
     done
done

# combine probability maps into one file
for hemi in L R; do
    bash process_sulci.sh group all $hemi
done

# get individual merged set of sulci after individual sulci have been created
subj_list='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 30 40 41 42 43 44 45 46 47 48 49 40 41 42 43 44 45 46 47 48 49 50'
for subj in $subj_list; do
    for hemi in L R; do
        bash process_sulci.sh $subj all $hemi
    done
done
