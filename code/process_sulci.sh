# call this from wrapper script process_sulci_wrapper.sh

subj=$1
sc=$2
hemi=$3
GD=${rootdir}/derivatives/group
subname=sub-${subj}
OD=${rootdir}/derivatives/${subname}
smoothing_kernel=2
threshold_vol_label=0.1
subj_list='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 30 40 41 42 43 44 45 46 47 48 49 40 41 42 43 44 45 46 47 48 49 50'
sc_list='cs_1 cs_2 cs_3 cs_4 cs_5 ascs_lat ascs_op pscs'

if [[ ! $subj == 'group' ]] ; then
    # get ventral motor cortex ROI from surface to volume
    if [[ $sc == 'ventral_ROI' ]]; then
        # extract ventral ROI border from combined border file and convert to metric file
        wb_command -border-to-rois $DD/T1w/Native/${subname}.${hemi}.midthickness.native.surf.gii  \
        $OD/sulci_native_surf/sulci_${hemi}.border $OD/sulci_native_surf/ventral_ROI_${hemi}.func.gii -border new_ventral_${hemi}

        # map metric surface file to volume
        wb_command -metric-to-volume-mapping $OD/sulci_native_surf/ventral_ROI_${hemi}.func.gii $OD/T1w/Native/${subname}.${hemi}.midthickness.native.surf.gii $OD/T1w/T1w_acpc_dc_restore_brain_1mm.nii.gz $OD/peaks_anat/ventral_ROI_mask_${hemi}.nii.gz \
        -ribbon-constrained $OD/T1w/Native/${subname}.${hemi}.white.native.surf.gii $OD/T1w/Native/${subname}.${hemi}.pial.native.surf.gii

    # get combined files after individual sulci have been processed
    elif [[ $sc == 'all' ]]; then
        # get all native ROIs from border file
        wb_command -border-to-rois $DD/T1w/Native/${subname}.${hemi}.midthickness.native.surf.gii  \
        $OD/sulci_native_surf/sulci_${hemi}.border $OD/sulci_native_surf/sulci_${hemi}.func.gii

        # merge specific set of sulci and borders for individual on fsaverage_LR32k sphere"
        cmd_str=""
        cmd_str_b=""
        for sc in $sc_list; do
            wb_command -set-map-names $OD/sulci_LR32k/${sc}_${hemi}.func.gii -map 1 ${sc}_${hemi}
            # generate border on fsaverage_LR32k sphere
            wb_command -metric-rois-to-border $DD/MNINonLinear/fsaverage_LR32k/${subname}.${hemi}.inflated.32k_fs_LR.surf.gii \
            $OD/sulci_LR32k/${sc}_${hemi}.func.gii ${subname} $OD/sulci_LR32k/${sc}_${hemi}.border
            cmd_str="$cmd_str -metric $OD/sulci_LR32k/${sc}_${hemi}.func.gii"
            cmd_str_b="$cmd_str_b -border $OD/sulci_LR32k/${sc}_${hemi}.border"
        done
        # combined file
        wb_command -metric-merge $OD/sulci_LR32k/sulci_${hemi}.func.gii $cmd_str
        wb_command -border-merge $OD/sulci_LR32k/sulci_${hemi}.border $cmd_str_b

    # process individual sulcus
    elif [[ ! $sc == 'all' ]] ; then
        sc_native=$OD/sulci_native_surf/${sc}_${hemi}.func.gii
        sc_LR32k=$OD/sulci_LR32k/${sc}_${hemi}.func.gii

        # get native surface ROIs from borders
        wb_command -border-to-rois $DD/T1w/Native/${subname}.${hemi}.midthickness.native.surf.gii  \
        $OD/sulci_native_surf/sulci_${hemi}.border $sc_native -border ${sc}_${hemi}
        wb_command -set-map-names $sc_native -map 1 ${sc}_${hemi}

        # map native surface ROIs to 0.7 mm acpc volume
        # use first subject's T1w as reference image, because all T1ws are in the same space
        wb_command -metric-to-volume-mapping $sc_native \
        $OD/T1w/Native/${subname}.${hemi}.pial.native.surf.gii \
        ${rootdir}/derivatives/sub-01/T1w/T1w_acpc_dc_restore.nii.gz \
        $OD/sulci_acpc_0.7mm/${sc}_${hemi}.nii.gz -nearest-vertex 1

        # resample sulcus from native to 32k sphere (without freesurfer registration)
        wb_command -metric-resample $sc_native \
        $OD/MNINonLinear/Native/sub-${subj}.${hemi}.sphere.native.surf.gii \
        $OD/MNINonLinear/fsaverage_LR32k/sub-${subj}.${hemi}.sphere.32k_fs_LR.surf.gii \
        ADAP_BARY_AREA $OD/sulci_common_sphere/${sc}_${hemi}.func.gii

        # resample sulcus from native to 32k_fs_LR based on freesurfer registration
        wb_command -metric-resample $sc_native \
        $OD/MNINonLinear/Native/${subname}.${hemi}.sphere.reg.reg_LR.native.surf.gii \
        $OD/MNINonLinear/fsaverage_LR32k/${subname}.${hemi}.sphere.32k_fs_LR.surf.gii \
        ADAP_BARY_AREA $sc_LR32k \
        -area-surfs $OD/T1w/Native/${subname}.${hemi}.midthickness.native.surf.gii \
        $OD/MNINonLinear/fsaverage_LR32k/${subname}.${hemi}.midthickness.32k_fs_LR.surf.gii \
        -current-roi $OD/MNINonLinear/Native/${subname}.${hemi}.roi.native.shape.gii

        # binarize resampled ROI to remove resampling artifacts at edges
        wb_command -metric-math "(x>0)" $sc_LR32k -var x $sc_LR32k
        # set map name
        wb_command -set-map-names $sc_LR32k -map 1 ${sc}_${hemi}

        # map ROI from 32k_fsaverage_LR mesh to 0.5 MNI volume
        wb_command -metric-to-volume-mapping $sc_LR32k \
        $OD/MNINonLinear/fsaverage_LR32k/${subname}.${hemi}.pial.32k_fs_LR.surf.gii \
        $FSLDIR/data/standard/MNI152_T1_0.5mm.nii.gz \
        $OD/sulci_LR32k/${sc}_${hemi}.nii.gz -nearest-vertex 1

        # smooth MNI volume sulcus
        fslmaths $OD/sulci_LR32k/${sc}_${hemi}.nii.gz -s ${smoothing_kernel} $OD/sulci_LR32k/${sc}_${hemi}_s.nii.gz

        # colour settings
        case $sc in
            cs_1 | cs_3 | cs_5)
            wb_command -metric-palette $OD/sulci_LR32k/${sc}_${hemi}.func.gii MODE_USER_SCALE -palette-name Gray_Interp_Positive -pos-user 0 0
            ;;
            cs_2 | cs_4)
            wb_command -metric-palette $OD/sulci_LR32k/${sc}_${hemi}.func.gii MODE_USER_SCALE -palette-name Gray_Interp_Positive -pos-user 0 4
            ;;
            ascs_lat)
            wb_command -metric-palette $OD/sulci_LR32k/${sc}_${hemi}.func.gii MODE_USER_SCALE -palette-name magma -pos-user 0 2
            ;;
            ascs_op)
            wb_command -metric-palette $OD/sulci_LR32k/${sc}_${hemi}.func.gii MODE_USER_SCALE -palette-name fsl_green -pos-user 0 4
            ;;
            pscs)
            wb_command -metric-palette $OD/sulci_LR32k/${sc}_${hemi}.func.gii MODE_USER_SCALE -palette-name blue-lightblue -pos-user 0 2
            ;;
        esac
    fi

# generate probability maps and collections of individual maps
elif [[  $subj == 'group' ]] ; then
    if [[ ! $sc == 'all' ]]; then
        TMPDIR=$(mktemp -d)
        # volumetric probability map
        prob_map_vol=$GD/sulci/${sc}_${hemi}_prob.nii.gz
        # surface probability map
        prob_map_surf=$GD/sulci/${sc}_${hemi}_prob.func.gii

        # VOLUME
        # make empty dummy volume for volume probability map
        fslmaths ${rootdir}/derivatives/sub-01/MNI/cs_1_${hemi}_s.nii.gz -mul 0 $prob_map_vol

        cmd_str_s=""
        cmd_str_v=""
        cmd_str_b=""
        for subj in $subj_list ; do
            subname=subj${s}
            OD=${rootdir}/derivatives/${subname}
            sc_vol=$OD/sulci_LR32k/${sc}_${hemi}_s.nii.gz
            sc_s=$OD/sulci_LR32k/${sc}_${hemi}.func.gii

            # threshold and binarize smoothed sulcus and then add to volume probability
            fslmaths $sc_vol -thr $threshold_vol_label $sc_vol
            fslmaths $sc_vol -bin $TMPDIR/${subj}${sc}${hemi}bin.nii.gz
            cmd_str_v="$cmd_str_v -add $TMPDIR/${subj}${sc}${hemi}bin.nii.gz"

            # add individual sulcus to surface probability map
            cmd_str_s="$cmd_str_s -metric $sc_s"
        done # subj

        # SURFACE
        # merge individual surface maps
        set_map_surf=$TMPDIR{sc}_${hemi}_individuals.func.gii
        wb_command -metric-merge $set_map_surf $cmd_str_s
        # make surface probability map
        wb_command -metric-reduce $set_map_surf SUM $prob_map_surf
        wb_command -set-map-names $prob_map_surf -map 1 $sc
        # make volumetric probability map
        fslmaths $prob_map_vol $cmd_str_v $prob_map_vol

    # combine all surface probability maps in a combined file"
    elif [[ $sc == 'all' ]]; then
        prob_map_surf_all=$GD/sulci/sulci_${hemi}_prob.func.gii
        cmd_str_all_s=""
        for sc in $sc_list; do
            prob_map_surf=$GD/sulci/${sc}_${hemi}_prob.func.gii
            cmd_str_all_s="$cmd_str_all_s -metric $prob_map_surf"
        done
        wb_command -metric-merge $prob_map_surf_all $cmd_str_all_s

        # combine all border files"
        border_all=$GD/sulci/sulci_${hemi}.border
        cmd_str_all_b=""
        for subj in $subj_list ; do
            subname=sub-${subj}
            OD=${rootdir}/derivatives/${subname}
            cmd_str_all_b="$cmd_str_all_b -border $OD/sulci_LR32k/sulci_${hemi}.border"
        done
        wb_command -border-merge $border_all $cmd_str_all_b

        # convert all metric labels to label file for better visualization
        for subj in $subj_list; do
          OD=${rootdir}/derivatives/sub-${subj}
          for hemi in L R; do
            wb_command -metric-math "(a + 2*b + 3*c+ 4*d + 5*e + 6*f + 7*g + 8*h)" $OD/sulci_native_surf/sulci_${hemi}.func.gii \
            -var a $OD/sulci_native_surf/cs_1_${hemi}.func.gii \
            -var b $OD/sulci_native_surf/cs_2_${hemi}.func.gii \
            -var c $OD/sulci_native_surf/cs_3_${hemi}.func.gii \
            -var d $OD/sulci_native_surf/cs_4_${hemi}.func.gii \
            -var e $OD/sulci_native_surf/cs_5_${hemi}.func.gii \
            -var f $OD/sulci_native_surf/ascs_lat_${hemi}.func.gii \
            -var g $OD/sulci_native_surf/ascs_op_${hemi}.func.gii \
            -var h $OD/sulci_native_surf/pscs_${hemi}.func.gii

            # change label names and exclude unused from overlaps
            wb_command -metric-label-import $OD/ROIs/methods_figure_${hemi}.func.gii \
            $GD/label_keys_open_data $OD/ROIs/sulci_${hemi}.label.gii -discard-others -drop-unused-labels
          done
        done
    fi
fi
