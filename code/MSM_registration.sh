# the script should be called from the wrapper script post_processing_wrapper.sh

# root directory where data is stored
rootdir='/myDir/project_larynx'
# group-level output directory
GD=$rootdir/derivatives/group
# outputs of the registrations
MSMD=$GD/MSM_registration

# subfunction to parse the input arguments
getoption() {
  sopt="--$1"
  shift 1
  for fn in $@ ; do
  	if [[ -n $(echo $fn | grep -- "^${sopt}=") ]] ; then
      echo $fn | sed "s/^${sopt}=//"
      return 0
    elif [[ -n $(echo $fn | grep -- "^${sopt}$") ]] ; then
      echo "TRUE" # if not string is given, set it the value of the option to TRUE
      return 0
  	fi
  done
}

hemi=$1
case $hemi in
    L) surf_hemi=CORTEX_LEFT ;;
    R) surf_hemi=CORTEX_RIGHT ;;
esac
subj=$2

# adapt derived data directory for the two groups of subjects
OD=$rootdir/derivatives/${subname}

subj_list_peaks='01 02 03 04 05 06 07 08 09 11 12 13 14 15 16 17 18 19 20 21'
subj_list_all='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 30 40 41 42 43 44 45 46 47 48 49 40 41 42 43 44 45 46 47 48 49 50'
sc_list_MSM='cs_1 cs_2 cs_3 cs_4 cs_5 ascs_lat ascs_op pscs'

# peaks from different effector types
peak_list='hand dorsal_larynx lip tongue ventral_larynx'

# different types of registration to resample peaks
reg_type_list='reg_sulc_depth reg_sulc_labels reg_FS'

# reference sphere shared for all registrations
ref_mesh=${rootdir}/derivatives/sub-01/MNINonLinear/fsaverage_LR32k/sub-01.${hemi}.sphere.32k_fs_LR.surf.gii

# general settings
thr_labels=0.4
levels=3

TMPDIR=$(mktemp -d)

# generate initial average of labels
if [[ $(getoption "avg_sulcal_labels" "$@") = "TRUE" ]] ; then
    MSM_sulci_avg=$MSMD/sulci_${hemi}_avg_0.func.gii
    cmd_str_all=""
    for sc in $sc_list_MSM; do
        echo $sc
        out_avg=$TMPDIR/${sc}_${hemi}_avg_0.func.gii
        cmd_str=""
        for subj in $subj_list_all; do
            OD=${rootdir}/derivatives/${subname}
            subname=sub-${subj}
            cmd_str="$cmd_str -metric $OD/sulci_common_sphere/${sc}_${hemi}.func.gii"
        done
        # average data of all subjects to generate the average file
        wb_command -metric-merge $out_avg $cmd_str
        # normalize so that map range from 0 to 1
        wb_command -metric-reduce $out_avg MEAN $out_avg
        # threshold normalized average to get a tighter ROI
        wb_command -metric-math "(x>$thr_labels)" $out_avg  -var x $out_avg
        cmd_str_all="$cmd_str_all -metric $out_avg"
    done
    # merge individual averages to combined file
    wb_command -metric-merge $MSM_sulci_avg $cmd_str_all
    echo $MSM_sulci_avg

# generate average of sulcal depth as template
elif [[ $(getoption "avg_sulc_depth" "$@") = "TRUE" ]] ; then
        MSM_sulc_depth_avg=$MSMD/sulc_${hemi}_avg_0.func.gii
        cmd_str_sulc=""
        for subj in $subj_list_all; do
          echo $subj
            OD=${rootdir}/derivatives/${subname}
            cmd_str_sulc="$cmd_str_sulc -metric $OD/MNINonLinear/fsaverage_LR32k/${subname}.${hemi}.sulc.32k_fs_LR.shape.gii"
        done
        # average data of all subjects to generate the average file
        wb_command -metric-merge $MSM_sulc_depth_avg $cmd_str_sulc
        wb_command -metric-reduce $MSM_sulc_depth_avg MEAN $MSM_sulc_depth_avg

# run MSM registration for sulcal labels
elif [[ $(getoption "run_MSM_sulci" "$@") = "TRUE" ]] ; then

    echo "running MSM for ${subname}.${hemi}.MSM_sulci.func.gii"
    # merge sulcal labels to create multimodal input file for MSM
    subject_sulci=$MSMD/MSM_sulcal_labels/${subname}.${hemi}.MSM_sulci.func.gii
    cmd_str=""
    for sc in $sc_list_MSM; do
        cmd_str="$cmd_str -metric $OD/sulci_native_surf/${sc}_${hemi}.func.gii"
    done
    wb_command -metric-merge $subject_sulci $cmd_str

    # run MSM
    outname=$MSMD/MSM_sulcal_labels/${subname}.${hemi}.
    msm --inmesh=$DD/MNINonLinear/Native/${subname}.${hemi}.sphere.native.surf.gii \
    --refmesh=$ref_mesh \
    --indata=$subject_sulci --refdata=$MSM_sulci_avg \
    --conf=$MSMD/conf_sulcal_labels --levels=$levels -o $outname

    echo "change map names"
    map=1
    for sc in $sc_list_MSM; do
        wb_command -set-map-names ${outname}transformed_and_reprojected.func.gii -map $map ${sc}_${hemi}
        map=$(($map + 1))
    done
    echo "done MSM"

# run MSM registration for sulcal depth maps
elif [[ $(getoption "run_MSM_sulc" "$@") = "TRUE" ]] ; then
    echo "run MSMsulc with default settings" $subname
    msm --inmesh=$OD/MNINonLinear/Native/${subname}.${hemi}.sphere.native.surf.gii \
    --refmesh=$ref_mesh \
    --indata=$OD/MNINonLinear/Native/${subname}.${hemi}.sulc.native.shape.gii \
    --refdata=$MSM_sulc_depth_avg \
    --levels=$levels -o $MSMD/MSM_sulc/${subname}.${hemi}. --trans=$OD/MNINonLinear/Native/${subname}.${hemi}.sphere.reg.reg_LR.native.surf.gii
    echo "done MSM sulcal depth"

# creage average of registered sulcal maps
elif [[ $(getoption "avg_sulci_post" "$@") = "TRUE" ]] ; then
    cmd_str_all=""
    cmd_str_b=""
    for sc in $sc_list_MSM; do
        out_avg=$TMPDIR/${sc}_${hemi}_avg_1.func.gii
        cmd_str=""
        for subj in $subj_list_all; do
            subname=sub-${subj}
            OD=~/scratch/LarynxRepresentation/derivatives/${subname}
            cmd_str="$cmd_str -metric $MSMD/MSM_sulcal_labels/${subname}.${hemi}.transformed_and_reprojected.func.gii -column ${sc}_${hemi}"
        done

        wb_command -metric-merge $out_avg $cmd_str
        wb_command -metric-reduce $out_avg MEAN $out_avg
        # threshold normalized average to get a tighter label
        wb_command -metric-math "(x>$thr_labels)" $out_avg -var x $out_avg
        cmd_str_all="$cmd_str_all -metric $out_avg"

        echo "convert metrics to get borders"
        wb_command -set-map-names $out_avg -map 1 ${sc}_${hemi}
        wb_command -metric-rois-to-border $ref_mesh $out_avg ${sc}_${hemi} $TMPDIR/${sc}_${hemi}.border
        cmd_str_b="$cmd_str_b -border $TMPDIR/${sc}_${hemi}.border"
    done

    wb_command -metric-merge $MSMD/sulci_${hemi}_avg_1.func.gii $cmd_str_all
    wb_command -border-merge $MSMD/sulci_${hemi}_avg_1.border $cmd_str_b
    wb_command -set-structure $MSMD/sulci_${hemi}_avg_1.border $surf_hemi

# resample functional peaks according to registrations
elif [[ $(getoption "resample" "$@") = "TRUE" ]] ; then
    for peak in $peak_list; do
        echo $peak
        # tongue
        if [ $peak = 'hand' ] ; then
          colour=fsl_yellow
        # lip
        elif [ $peak = 'dorsal_larynx' ] ; then
            colour=fsl_blue
        # vowel
        elif [ $peak = 'lip' ] ; then
            colour=fsl_green
        # phonation
        elif [ $peak = 'tongue' ]; then
            colour=fsl_red
        elif [ $peak = 'ventral' ]; then
            colour=blue-lightblue
        fi

        # smooth max so that is not lost during resampling
        wb_command -metric-smoothing $MSMD/MSM_sulcal_labels/${subname}.${hemi}.sphere.reg.surf.gii \
        $OD/peaks_acpc/${peak}_${hemi}_max.func.gii 1 \
        $TMPDIR/${subname}_${hemi}_${peak}_${hemi}_peak.func.gii

        # registration based on sulcal labels
        wb_command -metric-resample $TMPDIR/${subname}_${hemi}_${peak}_${hemi}_peak.func.gii \
        $MSMD/MSM_sulcal_labels/${subname}.${hemi}.sphere.reg.surf.gii \
        $ref_mesh BARYCENTRIC $OD/reg_sulc_labels/${peak}_${hemi}_max.func.gii

        # registration based on sulcal depth
        wb_command -metric-resample $TMPDIR/${subname}_${hemi}_${peak}_${hemi}_peak.func.gii \
        $MSMD/MSM_sulc/${subname}.${hemi}.sphere.reg.surf.gii \
        $ref_mesh BARYCENTRIC $OD/reg_sulc_deph/${peak}_${hemi}_max.func.gii

        # register based on FreeSurfer (obtained from FS pipeline)
        wb_command -metric-resample $TMPDIR/${subname}_${hemi}_${peak}_${hemi}_peak.func.gii \
        $OD/MNINonLinear/Native/sub-${subj}.${hemi}.sphere.reg.reg_LR.native.surf.gii \
        $ref_mesh BARYCENTRIC $OD/reg_FS/${peak}_${hemi}_max.func.gii

        for reg_type in $reg_type_list; do
          max=`wb_command -metric-stats $OD/${reg_type}/${peak}_${hemi}_max.func.gii -reduce MAX`
          wb_command -metric-math "(x==$max)" $OD/${reg_type}/${peak}_${hemi}_max.func.gii  -var x $OD/${reg_type}/${peak}_${hemi}_max.func.gii
          wb_command -metric-smoothing $ref_mesh $OD/${reg_type}/${peak}_${hemi}_max.func.gii 1 $OD/${reg_type}/${peak}_${hemi}_peak.func.gii
          wb_command -metric-math "(x>0)" $OD/${reg_type}/${peak}_${hemi}_peak.func.gii -var x $OD/${reg_type}/${peak}_${hemi}_peak.func.gii
          # colour setting
          wb_command -metric-palette $OD/${reg_type}/${peak}_${hemi}_peak.func.gii MODE_AUTO_SCALE_PERCENTAGE -palette-name $colour
        done
    done

elif [[ $subj == 'group' ]] ; then
    for reg_type in $reg_type_list; do
      cmd_str_all=""
      for peak in $peak_list; do
          if [ $peak = 'hand' ] ; then
            colour=fsl_yellow
          elif [ $peak = 'dorsal' ] ; then
              colour=fsl_blue
          elif [ $peak = 'lip' ] ; then
              colour=fsl_green
          elif [ $peak = 'tongue' ]; then
              colour=fsl_red
          elif [ $peak = 'ventral' ]; then
              colour=blue-lightblue
          fi

          cmd_str=""
          for subj in $subj_list_peaks; do
              OD=${rootdir}/derivatives/sub-$subj
              ls $OD/${reg_type}/${peak}_${hemi}_peak.func.gii
              wb_command -set-structure $OD/${reg_type}/${peak}_${hemi}_peak.func.gii $surf_hemi
              cmd_str="$cmd_str -metric $OD/${reg_type}/${peak}_${hemi}_peak.func.gii"
          done

          peaks_added=$GD/${reg_type}/${peak}_${hemi}_added.func.gii
          wb_command -metric-merge $peaks_added $cmd_str
          wb_command -metric-reduce $peaks_added SUM $peaks_added
          wb_command -metric-palette $peaks_added MODE_AUTO_SCALE_PERCENTAGE -palette-name $colour

          cmd_str_all="$cmd_str_all -metric $peaks_added"
          echo "done $peaks_added"

      done # peak
      wb_command -metric-merge $GD/${reg_type}/peaks_${hemi}_added.func.gii $cmd_str_all
    echo "done add MSM peaks"
    done
fi