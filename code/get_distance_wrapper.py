import numpy as np
import os
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from functools import reduce

run_or_show = sys.argv[1]

rootdir = os.path.join(os.sep, 'Users', 'neichert', 'public_data', 'project_larynx')
GD = os.path.join(rootdir, 'derivatives', 'group')
subj_list = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']

type_list = ['FS', 'sulc_depth', 'sulc_labels']
hemi_list = ['L', 'R']
colour_list = np.array([[0.,   0.6,  1.  ],
                         [0.,   0.7,  0.  ],
                         [0.,   0.63, 0.68],
                         [0.,   0.67, 0.34],
                         [0.68, 0.4,  1.  ],
                         [0.68, 0.5,  0.  ],
                         [1.,   0.33, 0.68],
                         [1.,   0.37, 0.34]])

df_ROIs = pd.DataFrame(columns=["long_name", "short_name", "sulcus_expected", "colour"])
df_ROIs.loc[len(df_ROIs)] = ['Judgement_cp7_hand', 'hand', 'cs_2', colour_list[5]]
df_ROIs.loc[len(df_ROIs)] = ['Factorial_cp8_dorsal', 'dorsal_larynx', 'cs_3', np.array([0, 0, 1])]
df_ROIs.loc[len(df_ROIs)] = ['ArtVoc_cp6_lip', 'lip', 'cs_3', colour_list[1]]
df_ROIs.loc[len(df_ROIs)] = ['ArtVoc_cp5_tongue', 'tongue', 'cs_4', colour_list[7]]
df_ROIs.loc[len(df_ROIs)] = ['Factorial_cp8_ventral', 'ventral_larynx', 'cs_5|ascs_lat', colour_list[0]]

# if run, get distance for all registrations from other script
if run_or_show == 'run':
    for hemi in hemi_list:
        for ind_r, ROI in df_ROIs.iterrows():
            for type in type_list:
                print("do:", hemi, ROI.short_name, type)
                os.system(f'python get_distance.py {hemi} {ROI.long_name} {type}')

# if 'show', plot results
elif run_or_show == 'load':
    # read in all distances in one dataframe
    df = pd.DataFrame(columns=['hemi', 'ROI', 'subject1', 'subject2', 'distance_FS', 'distance_sulc_depth', 'distance_sulc_labels'])
    for hemi in hemi_list:
        for ind_r, ROI in df_ROIs.iterrows():
            dfList = []
            for type in type_list:
                print(f'{ROI.short_name} {hemi} {type}')
                # read in calculated distances
                distances_fname = os.path.join(GD, 'func', f'{ROI.long_name}_{type}_{hemi}_distances.pkl')
                df_dist = pd.read_pickle(distances_fname)
                df_dist.rename(columns={"distance": f'distance_{type}'}, inplace=1)
                dfList.append(df_dist)
            df_dist = reduce(lambda x, y: pd.merge(x, y, on=['subject1', 'subject2']), dfList)
            df_dist['hemi'] = hemi
            df_dist['ROI'] = ROI.short_name
            df = pd.concat([df, df_dist], sort=False)

    # exclude hand in RH
    df = df[~((df.ROI == 'hand') & (df.hemi == "R"))]

    # get distances subject-by-subject
    df_s = pd.DataFrame(columns=['hemi', 'ROI', 'subject1', 'subject2'])
    stored_combis = []
    for subj1 in subj_list:
        for subj2 in subj_list:
            if not subj1 == subj2:
                # only continue if this pair of subjects has not been added
                go_on = 1
                my_combi = [subj1, subj2]
                for i in stored_combis:
                    if set(my_combi) == set(i):
                        go_on = 0
                if go_on:
                    stored_combis.append(my_combi)
                    df_sub = df[(df['subject1'] == subj1) & (df['subject2'] == subj2)]
                    df_s = df_s.append(df_sub, ignore_index=True)

    distList = [i for i in df.columns.to_list() if i.startswith('distance_')]
    df_reshape = pd.melt(df_s, id_vars=['subject1', 'subject2', 'hemi', 'ROI'], value_vars=distList)
    df_reshape.rename(columns={"value": "distance", "variable": "registration_type"}, inplace=True)
    df_reshape.to_csv(GD, 'anat', 'sulci', 'peak_distances.csv')

elif run_or_show == 'plot':

    # PLOTTING
    # load here from disk
    df = pd.read_csv(os.path.join(GD, 'anat', 'sulci', 'peak_distances.csv'))

    # plot average distances
    colours = [tuple(col) for col in df_ROIs.colour]
    colours_i, indexes = np.unique(colours, axis=0, return_index=True)
    col_list = [colours[index] for index in sorted(indexes)]

    p1 = sns.catplot(x="ROI", estimator=np.median, order=df_ROIs.short_name.to_list(), y="distance", data=df, hue='registration_type', hue_order=['FS', 'sulcal_depth', 'sulcal_labels'], col='hemi', kind='bar', palette=sns.set_palette(col_list), legend=False, height=3, aspect=1, ci=95)
    for ax in p1.axes[0]:
        for ind_ROI, colour in enumerate(col_list):
            ax.patches[ind_ROI].set_facecolor(colour)
            ax.patches[ind_ROI + 5].set_facecolor(colour)
            ax.patches[ind_ROI + 5].set_hatch('///')
            ax.patches[ind_ROI + 10].set_facecolor(colour)
            ax.patches[ind_ROI + 10].set_hatch('o')
            ax.tick_params(axis='both', which='major', labelsize=15)

    plt.show()