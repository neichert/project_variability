# subject IDs of subjects that provided functional data
subj_list_peaks='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20'

# subject IDs of both subject pools
subj_list_all='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 30 40 41 42 43 44 45 46 47 48 49 40 41 42 43 44 45 46 47 48 49 50'

# create initial average as target
for hemi in L R; do
    bash MSM_registration.sh ${hemi} --avg_sulc_depth
    bash MSM_registration.sh ${hemi} --avg_sulcal_labels
done


# run MSM on sulcal labels for all subjects
for hemi in L R; do
    for subj in $subj_list_all; do
        bash MSM_registration.sh ${hemi} $subj --run_MSM_sulci
    done
done

# run MSM on sulcal depth for all subjects
for hemi in L R; do
    for subj in $subj_list_all; do
        bash MSM_registration.sh ${hemi} $subj --run_MSM_sulc
    done
done

# get averages of resampled sulci
for hemi in L R; do
    bash MSM_registration.sh ${hemi} --avg_sulci_post
done

# change colours of borders
python set_colour_labels.py group

# resample peaks according to registrations
for hemi in L R; do
    for subj in $subj_list_peaks; do
        bash MSM_registration.sh ${hemi} $subj --resample
    done
done

# add individual peaks to one metric file
for hemi in L R; do
    bash MSM_registration.sh ${hemi} group
done

# quantify distances of peaks
python get_distance_wrapper.py run
python get_distance_wrapper.py show
